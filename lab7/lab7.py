# MIT 6.034 Lab 7: Support Vector Machines
# Written by 6.034 staff

from svm_data import *
from functools import reduce
import math

#### Part 1: Vector Math #######################################################

def dot_product(u, v):
    """Computes the dot product of two vectors u and v, each represented 
    as a tuple or list of coordinates. Assume the two vectors are the
    same length."""
    dot =0 
    for i in range(len(u)):
        dot+= u[i]*v[i]
    return dot

def norm(v):
    """Computes the norm (length) of a vector v, represented 
    as a tuple or list of coords."""
    n =0 
    for i in range(len(v)):

        n+= v[i]**2
    return math.sqrt(n)

#### Part 2: Using the SVM Boundary Equations ##################################

def positiveness(svm, point):
    """Computes the expression (w dot x + b) for the given Point x."""
    return  dot_product(svm.w,point.coords) + svm.b
    # raise NotImplementedError

def classify(svm, point):
    """Uses the given SVM to classify a Point. Assume that the point's true
    classification is unknown.
    Returns +1 or -1, or 0 if point is on boundary."""
    if positiveness(svm, point) > 0:
        return 1
    elif positiveness(svm, point) < 0:
        return -1
    else:
        return 0
    # raise NotImplementedError

def margin_width(svm):
    """Calculate margin width based on the current boundary."""
    return 2/norm(svm.w)
    # raise NotImplementedError

def check_gutter_constraint(svm):
    """Returns the set of training points that violate one or both conditions:
        * gutter constraint (positiveness == classification, for support vectors)

        * training points must not be between the gutters
    Assumes that the SVM has support vectors assigned."""
    a = set()
    for i in svm.support_vectors:
        print(i)
        print(i.classification)
        print(classify(svm, i))
        if positiveness(svm, i) != i.classification:
            a.add(i)
    for j in svm.training_points:
        print(j)
        if positiveness(svm, j)>-1 and positiveness(svm, j)<1:
            a.add(j)
        # if positiveness(svm, i) < margin_width(svm):
        #     a.add(i)
        
    return a


#### Part 3: Supportiveness ####################################################

def check_alpha_signs(svm):
    """Returns the set of training points that violate either condition:
        * all non-support-vector training points have alpha = 0
        * all support vectors have alpha > 0
    Assumes that the SVM has support vectors assigned, and that all training
    points have alpha values assigned."""
    a = set()
    b = set()
    for j in svm.support_vectors:
        b.add(j)
        if j.alpha<=0:
            a.add(j)

    for i in svm.training_points:
            if i not in b:
                if i.alpha != 0:
                    a.add(i)
    return a
def check_alpha_equations(svm):
    """Returns True if both Lagrange-multiplier equations are satisfied,
    otherwise False. Assumes that the SVM has support vectors assigned, and
    that all training points have alpha values assigned."""
    a = 0
    b = []
    print(len(svm.w))
    for k in range(len(svm.w)):
        b.append(0)
    print(b)
    for i in svm.training_points:
        print(len(i.coords))
        a+= i.classification*i.alpha
        b = vector_add(scalar_mult((i.classification*i.alpha), i.coords), b)
    print(type(b))

    if a == 0 and b == svm.w:
        return True
    else:
        return False


#### Part 4: Evaluating Accuracy ###############################################

def misclassified_training_points(svm):
    """Returns the set of training points that are classified incorrectly
    using the current decision boundary."""
    a = set()
    for i in svm.training_points:
        if classify(svm, i) != i.classification:
            a.add(i)
    return a


#### Part 5: Training an SVM ###################################################

def update_svm_from_alphas(svm):
    """Given an SVM with training data and alpha values, use alpha values to
    update the SVM's support vectors, w, and b. Return the updated SVM."""
    #get the w from equation 4
    #the get the biases 
    #then average the biases
    #finally update the orginal svm and return 
    weights = []
    #print(len(svm.w))
    for k in range(len(svm.training_points[0])):
        weights.append(0)
    #print(weights)
    svec = []
    for i in svm.training_points:
        if i.alpha>0:
            svec.append(i)
    for j in svec:
        weights = vector_add(scalar_mult((j.classification*j.alpha), j.coords), weights)
    psvec = []
    nsvec = []
    for k in svec:
        if k.classification > 0:
            psvec.append(k.classification- dot_product(weights, k.coords))

        else:
            nsvec.append(k.classification- dot_product(weights, k.coords))
    print(len(svm.support_vectors))
    for i in svec:
        if i not in svm.support_vectors:
            if i.alpha>0:
                svm.support_vectors.append(i)
    for j in svm.support_vectors:
        if j.alpha<=0:
            svm.support_vectors.remove(j)
    b = (max(psvec) + min(nsvec))/2
    svm = svm.set_boundary(weights, b)
    return svm



#### Part 6: Multiple Choice ###################################################

ANSWER_1 = 11
ANSWER_2 = 6
ANSWER_3 = 3
ANSWER_4 = 2

ANSWER_5 = ['A', 'D']
ANSWER_6 = ['A', 'B', 'D']
ANSWER_7 = ['A', 'B', 'D']
ANSWER_8 = []
ANSWER_9 = ['A', 'B', 'D']
ANSWER_10 =['A', 'B', 'D']

ANSWER_11 = False
ANSWER_12 = True
ANSWER_13 = False
ANSWER_14 = False
ANSWER_15 = False
ANSWER_16 = True

ANSWER_17 = [1,3,6,8]
ANSWER_18 = [1,2,4,5,6,7,8]
ANSWER_19 = [1,2,4,5,6,7,8]

ANSWER_20 = 6


#### SURVEY ####################################################################

NAME = "Gaurav Chandra"
COLLABORATORS = ""
HOW_MANY_HOURS_THIS_LAB_TOOK = 6
WHAT_I_FOUND_INTERESTING = ""
WHAT_I_FOUND_BORING = ""
SUGGESTIONS = ""
