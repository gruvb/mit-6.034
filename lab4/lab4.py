# MIT 6.034 Lab 4: Constraint Satisfaction Problems
# Written by 6.034 staff

from constraint_api import *
from test_problems import get_pokemon_problem


#### Part 1: Warmup ############################################################

def has_empty_domains(csp) :
    """Returns True if the problem has one or more empty domains, otherwise False"""
    for i in csp.domains:
      #  print(csp.domains[i])
        if len(csp.domains[i]) == 0:
            return True
    return False

def check_all_constraints(csp) :
    """Return False if the problem's assigned values violate some constraint,
    otherwise True"""
    for i in csp.constraints:
        i1 = i.var1     
        i2 = i.var2
        #if i1, i2 in csp.assignments:
        if i1 in csp.assignments and i2 in csp.assignments:
            i11 = csp.get_assignment(i1)
            i22 = csp.get_assignment(i2)
            if i.check(i11, i22) == False:
              return False
    return True

#### Part 2: Depth-First Constraint Solver #####################################

def solve_constraint_dfs(problem) :
    """
    Solves the problem using depth-first search.  Returns a tuple containing:
    1. the solution (a dictionary mapping variables to assigned values)
    2. the number of extensions made (the number of problems popped off the agenda).
    If no solution was found, return None as the first element of the tuple.
    """
    agenda = [problem]
    print(agenda)
    extension = 0
    while len(agenda) > 0:
        i = agenda.pop(0)
        extension += 1
        #print(i)
        #print(i.domains)
        if has_empty_domains(i) or not(check_all_constraints(i)):
            continue
        else:
            if len(i.unassigned_vars) == 0:
                return (i.assignments, extension)
            else:
                p = []
                j = i.pop_next_unassigned_var()
                newdomain = i.get_domain(j)
                #print(newdomain)
                for n in newdomain:
                    ii = i.copy()
                    newproblem = ii.set_assignment(j, n)
                    p.append(newproblem)
                agenda += p

    return (None, extension)


# QUESTION 1: How many extensions does it take to solve the Pokemon problem
#    with DFS?

# Hint: Use get_pokemon_problem() to get a new copy of the Pokemon problem
#    each time you want to solve it with a different search method.

ANSWER_1 = 20


#### Part 3: Forward Checking ##################################################

def eliminate_from_neighbors(csp, var) :
    """
    Eliminates incompatible values from var's neighbors' domains, modifying
    the original csp.  Returns an alphabetically sorted list of the neighboring
    variables whose domains were reduced, with each variable appearing at most
    once.  If no domains were reduced, returns empty list.
    If a domain is reduced to size 0, quits immediately and returns None.
    """
    r = []
    for i in csp.get_neighbors(var):
        b = []
        flag = len(csp.get_domain(i))
        #print(csp.get_domain(i))
        for j in csp.get_domain(i):
            check = 0
            count =0 
            count1 = len(csp.get_domain(var))
            #print(csp.get_domain(var))
            for k in csp.get_domain(var):
                a = []
                for l in csp.constraints_between(var,i):
                    if l.check(k,j) == True:
                        a.append("1")            
                if len(a) == len(csp.constraints_between(var, i)):
                    check = 1
            if check ==0:
                b.append(j)


        # if len(b)>0:
        for m in b:
            csp.eliminate(i,m)
            r.append(i)
        if not(csp.get_domain(i)):
            return None

    d = set()
    for ii in r:
        d.add(ii)
    e = list(d)
    ee = sorted(d)
    return ee

# Because names give us power over things (you're free to use this alias)
forward_check = eliminate_from_neighbors

def solve_constraint_forward_checking(problem) :
    """
    Solves the problem using depth-first search with forward checking.
    Same return type as solve_constraint_dfs.
    """
    raise NotImplementedError


# QUESTION 2: How many extensions does it take to solve the Pokemon problem
#    with DFS and forward checking?

ANSWER_2 = 20


#### Part 4: Domain Reduction ##################################################

def domain_reduction(csp, queue=None) :
    """
    Uses constraints to reduce domains, propagating the domain reduction
    to all neighbors whose domains are reduced during the process.
    If queue is None, initializes propagation queue by adding all variables in
    their default order. 
    Returns a list of all variables that were dequeued, in the order they
    were removed from the queue.  Variables may appear in the list multiple times.
    If a domain is reduced to size 0, quits immediately and returns None.
    This function modifies the original csp.
    """
    print(type(queue))

    r = []
    if queue is not None:
        a = queue
    else:
        a = csp.get_all_variables() 
    while len(a)>0:
        i = a.pop(0)
        r.append(i)
        m = eliminate_from_neighbors(csp, i)
        if m:
            for j in m:
                if j not in a:
                    a.append(j)
    return r


# QUESTION 3: How many extensions does it take to solve the Pokemon problem
#    with DFS (no forward checking) if you do domain reduction before solving it?

ANSWER_3 = 20


def solve_constraint_propagate_reduced_domains(problem) :
    """
    Solves the problem using depth-first search with forward checking and
    propagation through all reduced domains.  Same return type as
    solve_constraint_dfs.
    """
    agenda = [problem]
    print(agenda)
    extension = 0
    while len(agenda) > 0:
        i = agenda.pop(0)
        extension += 1
        #print(i)
        #print(i.domains)
        if has_empty_domains(i) or not(check_all_constraints(i)):
            continue
        else:
            if len(i.unassigned_vars) == 0:
                return (i.assignments, extension)
            else:
                p = []
                j = i.pop_next_unassigned_var()
                newdomain = i.get_domain(j)
                #print(newdomain)
                for n in newdomain:
                    ii = i.copy()
                    newproblem = ii.set_assignment(j, n)
                    domain_reduction(newproblem, [j])
                    p.append(newproblem)
                agenda += p

    return (None, extension)


# QUESTION 4: How many extensions does it take to solve the Pokemon problem
#    with forward checking and propagation through reduced domains?

ANSWER_4 = 20


#### Part 5A: Generic Domain Reduction #########################################

def propagate(enqueue_condition_fn, csp, queue=None) :
    """
    Uses constraints to reduce domains, modifying the original csp.
    Uses enqueue_condition_fn to determine whether to enqueue a variable whose
    domain has been reduced. Same return type as domain_reduction.
    """
    print(enqueue_condition_fn)
    r = []
    if queue is not None:
        a = queue
    else:
        a = csp.get_all_variables() 
    while len(a)>0:
        i = a.pop(0)
        r.append(i)
        m = eliminate_from_neighbors(csp, i)
        if m:
            for j in m:
                if j not in a:
                    if enqueue_condition_fn(csp, j) == True:
                        a.append(j)
    return r


def condition_domain_reduction(csp, var) :
    """Returns True if var should be enqueued under the all-reduced-domains
    condition, otherwise False"""
    return True

def condition_singleton(csp, var) :
    """Returns True if var should be enqueued under the singleton-domains
    condition, otherwise False"""
    if len(csp.get_domain(var)) == 1:
        return True
    else:
        return False

def condition_forward_checking(csp, var) :
    """Returns True if var should be enqueued under the forward-checking
    condition, otherwise False"""
    return False


#### Part 5B: Generic Constraint Solver ########################################

def solve_constraint_generic(problem, enqueue_condition=None) :
    """
    Solves the problem, calling propagate with the specified enqueue
    condition (a function). If enqueue_condition is None, uses DFS only.
    Same return type as solve_constraint_dfs.
    """
    agenda = [problem]
    print(agenda)
    extension = 0
    while len(agenda) > 0:
        i = agenda.pop(0)
        extension += 1
        #print(i)
        #print(i.domains)
        if has_empty_domains(i) or not(check_all_constraints(i)):
            continue
        else:
            if len(i.unassigned_vars) == 0:
                return (i.assignments, extension)
            else:
                p = []
                j = i.pop_next_unassigned_var()
                newdomain = i.get_domain(j)
                #print(newdomain)
                for n in newdomain:
                    ii = i.copy()
                    newproblem = ii.set_assignment(j, n)
                    if enqueue_condition is not None:
                        propagate(enqueue_condition, newproblem, [j])
                    p.append(newproblem)
                agenda += p

    return (None, extension) 

# QUESTION 5: How many extensions does it take to solve the Pokemon problem
#    with forward checking and propagation through singleton domains? (Don't
#    use domain reduction before solving it.)

ANSWER_5 = 20


#### Part 6: Defining Custom Constraints #######################################

def constraint_adjacent(m, n) :
    """Returns True if m and n are adjacent, otherwise False.
    Assume m and n are ints."""
    if abs(m-n) == 1:
        return True
    else:
        return False

def constraint_not_adjacent(m, n) :
    """Returns True if m and n are NOT adjacent, otherwise False.
    Assume m and n are ints."""
    flag= not constraint_adjacent(m,n)
    print(flag)
    return flag

def all_different(variables) :
    """Returns a list of constraints, with one difference constraint between
    each pair of variables."""
    a = []
    print(type(variables))
    return a


#### SURVEY ####################################################################

NAME = "Gaurav Chandra"
COLLABORATORS = ""
HOW_MANY_HOURS_THIS_LAB_TOOK = 10
WHAT_I_FOUND_INTERESTING = ""
WHAT_I_FOUND_BORING = ""
SUGGESTIONS = ""