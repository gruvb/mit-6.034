# MIT 6.034 Lab 9: Boosting (Adaboost)
# Written by 6.034 staff

from math import log as ln
from utils import *


#### Part 1: Helper functions ##################################################

def initialize_weights(training_points):
    """Assigns every training point a weight equal to 1/N, where N is the number
    of training points.  Returns a dictionary mapping points to weights."""
    a = {}
    for i in training_points:
        a.update({i:make_fraction(1, len(training_points))})
    return a

def calculate_error_rates(point_to_weight, classifier_to_misclassified):
    """Given a dictionary mapping training points to their weights, and another
    dictionary mapping classifiers to the training points they misclassify,
    returns a dictionary mapping classifiers to their error rates."""
    a = {}
    # print(point_to_weight)
    # print(classifier_to_misclassified)
    for i in classifier_to_misclassified.keys():
        a[i] = 0
        for j in classifier_to_misclassified.get(i):
            a[i]+=point_to_weight.get(j)
            # else:
            # # a[i] = point_to_weight[i]
            #     for j in i:
            #         b = point_to_weight.get(j)
            #         print(b)
            #         a.update({i:b})

    return a

def pick_best_classifier(classifier_to_error_rate, use_smallest_error=True):
    """Given a dictionary mapping classifiers to their error rates, returns the
    best* classifier, or raises NoGoodClassifiersError if best* classifier has
    error rate 1/2.  best* means 'smallest error rate' if use_smallest_error
    is True, otherwise 'error rate furthest from 1/2'."""
    e = 1
    for i in classifier_to_error_rate:
        print(i)
        print(classifier_to_error_rate.get(i))
    # print(classifier_to_error_rate)
    if use_smallest_error == True:
        for i in classifier_to_error_rate.keys():
            a = make_fraction(1,2)
            # print(a)
            if classifier_to_error_rate.get(i)<e:
                e = classifier_to_error_rate.get(i)
                idx = i

    else:
        print("else")
        for i in classifier_to_error_rate.keys():
            a = make_fraction(1,2)
            if classifier_to_error_rate.get(i)>a:
                b = classifier_to_error_rate.get(i)
                b = 1 - b
            # print(a)
                if b<e:
                    e = b
                    idx = i
            else:

                b = classifier_to_error_rate.get(i)
                if b<e:
                    e = b
                    idx = i
    print(e)
    if e == make_fraction(1, 2):
        print("else")
        raise NoGoodClassifiersError()


    return idx


    # raise NotImplementedError

def calculate_voting_power(error_rate):
    """Given a classifier's error rate (a number), returns the voting power
    (aka alpha, or coefficient) for that classifier."""
    if error_rate == 1:
        return -INF
    if error_rate == 0:
        return INF
    return make_fraction(1,2)*ln(make_fraction(1-error_rate, error_rate))
    # raise NotImplementedError

def get_overall_misclassifications(H, training_points, classifier_to_misclassified):
    """Given an overall classifier H, a list of all training points, and a
    dictionary mapping classifiers to the training points they misclassify,
    returns a set containing the training points that H misclassifies.
    H is represented as a list of (classifier, voting_power) tuples."""
    # print(H)
    # a = set()
    # for i in classifier_to_misclassified.keys():
    #     for j in classifier_to_misclassified.get(i):
    #         a.add(j)
    # return a
    # for i in training_points:
    #     for j in H:
    #         if j[0] in classifier_to_misclassified.keys():
                #logic
    #majority of points getting misclassified would result in H being negative
    #first assign a dictionary- element:0
    a = {}
    for i in training_points:
        a[i] =0
    used_classifiers = set()
    #keep on negating the voting power.
    for i in training_points:
        # for key, value in classifier_to_misclassified.items():
        #     # print(value)
        #     # l = value
        #     for j in value:

        #         if j == i:
                    # b =  key
        for k in H:
            p = classifier_to_misclassified[k[0]]
            # print(k)
            if i in p:
                a[i]-=k[1]
            else:
                a[i]+=k[1]
                used_classifiers.add(k[0])
    # print(a)
    result = set()
    # # print(used_classifiers)
    # for i in training_points:
    #     for j in H:
    #         if j[0] not in used_classifiers:
    #             a[i]+=j[1]
    for key, value in a.items():
        if value<=0:
            result.add(key)
                    # if key != j[0]:
                    #     count+=1
        # if count == lens(H):

    # for j in classifier_to_misclassified.keys():


    #     if j in classifier_to_misclassified.keys():

    #         a[j] = 
    #         if classifier_to_misclassified.get(j[0]) not in a:
    #             a[classifier_to_misclassified.get(j[0])] -= j[1]
    #     a[classifier_to_misclassified.get(j[0])] += j[1]
    #         count-=j[1]
    #     else:ß
    #         count+=j[1]


    return result

def is_good_enough(H, training_points, classifier_to_misclassified, mistake_tolerance=0):
    """Given an overall classifier H, a list of all training points, a
    dictionary mapping classifiers to the training points they misclassify, and
    a mistake tolerance (the maximum number of allowed misclassifications),
    returns False if H misclassifies more points than the tolerance allows,
    otherwise True.  H is represented as a list of (classifier, voting_power)
    tuples."""
    a = {}
    for i in training_points:
        a[i] =0
    used_classifiers = set()
    #keep on negating the voting power.
    count = 0
    tie = []
    for i in training_points:
        b = set()
        for k in H:
            p = classifier_to_misclassified[k[0]]
            # print(k)
            if i in p:
                a[i]-=k[1]
            else:
                a[i]+=k[1]
                used_classifiers.add(k[0])
        # for l in range(1,len(H)):
        #     if H[l-1][1]== H[l][1]:
        #         count+=1
        # print(b)
        if a[i]<0:
            count+=1

    # print(a)
    for j in a.values():
        if j == 0:
            count+=1
        # b.add(a[i])
        # tie.append(list(b)[0])
        # print(tie)
    # print(tie)
    # if len(tie) != len(set(tie)):
    #     count-=1
    # if a[i] == 0:
    #     count+=1
    if count>mistake_tolerance:
        return False
    else:
        return True

def update_weights(point_to_weight, misclassified_points, error_rate):
    """Given a dictionary mapping training points to their old weights, a list
    of training points misclassified by the current weak classifier, and the
    error rate of the current weak classifier, returns a dictionary mapping
    training points to their new weights.  This function is allowed (but not
    required) to modify the input dictionary point_to_weight."""
    a = {}
    for i,j in point_to_weight.items():
        if i in misclassified_points:
            a[i] = make_fraction(0.5*make_fraction(1, error_rate)*j)
        else:
            a[i] = make_fraction(0.5*make_fraction(1,(1-error_rate))*j)
    return a




#### Part 2: Adaboost ##########################################################

def adaboost(training_points, classifier_to_misclassified,
             use_smallest_error=True, mistake_tolerance=0, max_rounds=INF):
    """Performs the Adaboost algorithm for up to max_rounds rounds.
    Returns the resulting overall classifier H, represented as a list of
    (classifier, voting_power) tuples."""
    # raise NotImplementedError
    H = []
    point_to_weight = initialize_weights(training_points)
    i =0 
    while i<max_rounds:
        classifier_to_error_rate = calculate_error_rates(point_to_weight, classifier_to_misclassified)
        try:
            pick_best_classifier(classifier_to_error_rate, use_smallest_error) ==make_fraction(1,2)
        except:
            return H

        best_classifier = pick_best_classifier(classifier_to_error_rate, use_smallest_error)

        error_rate = make_fraction(classifier_to_error_rate[best_classifier])
        voting_power = calculate_voting_power(error_rate)
        H.append((best_classifier, voting_power))
        misclassified_points = classifier_to_misclassified[best_classifier]
        if is_good_enough(H, training_points, classifier_to_misclassified, mistake_tolerance):
            return H
        point_to_weight = update_weights(point_to_weight, misclassified_points, error_rate)
        i+=1
    return H

#### SURVEY ####################################################################

NAME = "Gaurav Chandra"
COLLABORATORS = ""
HOW_MANY_HOURS_THIS_LAB_TOOK = 6
WHAT_I_FOUND_INTERESTING = ""
WHAT_I_FOUND_BORING = ""
SUGGESTIONS = ""
