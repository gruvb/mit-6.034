 # MIT 6.034 Lab 0: Getting Started
# Written by jb16, jmn, dxh, and past 6.034 staff

from point_api import Point

#### Multiple Choice ###########################################################

# These are multiple choice questions. You answer by replacing
# the symbol 'None' with a letter (or True or False), corresponding 
# to your answer.

# True or False: Our code supports both Python 2 and Python 3
# Fill in your answer in the next line of code (True or False):
ANSWER_1 = False

# What version(s) of Python do we *recommend* for this course?
#   A. Python v2.3
#   B. Python V2.5 through v2.7
#   C. Python v3.2 or v3.3
#   D. Python v3.4 or higher
# Fill in your answer in the next line of code ("A", "B", "C", or "D"):
ANSWER_2 = "D"


################################################################################
# Note: Each function we require you to fill in has brief documentation        # 
# describing what the function should input and output. For more detailed      # 
# instructions, check out the lab 0 wiki page!                                 #
################################################################################


#### Warmup ####################################################################

def is_even(x):
    """If x is even, returns True; otherwise returns False"""
    if x%2==0:
        return True
    else:
        return False

def decrement(x):
    """Given a number x, returns x - 1 unless that would be less than
    zero, in which case returns 0."""
    if x<0:
        return 0
    else:
        return x-1 

def cube(x):
    """Given a number x, returns its cube (x^3)"""
    return x*x*x


#### Iteration #################################################################

def is_prime(x):
    """Given a number x, returns True if it is prime; otherwise returns False"""
    if x<=0:
        return False
    if x ==2:
        return True
    if(type(x)==float):
        return False
    if x>2 :
        flag =0
        for i in range(1,x):
            #for j in range(i):
                if x%i!=0:
                    flag+=1
        if flag==x-2:
            return True
        else:
            return False  

def primes_up_to(x):
    primes = []
    #if x==2:
     #   primes.append(2)
    if(type(x)==float):
        x = int(x)
        #print(x)
    for i in range(1,x):
        if(is_prime(i)):
           primes.append(i)
    if(is_prime(x)):
        primes.append(x)
    return primes

#print(primes_up_to(2))
#primes = primes_up_to(x)
#print(primes)




#### Recursion #################################################################

def fibonacci(n):
    #t1 =1, t2 =1
    if n<0:
        raise Exception("fibonacci: input must not be negative")
    if n == 1:
        return 1
    if n==2:
        return 1
    if n>2:
        return fibonacci(n-1) + fibonacci(n-2) 


def expression_depth(expr):
    """Given an expression expressed as Python lists, uses recursion to return
    the depth of the expression, where depth is defined by the maximum number of
    nested operations."""

    raise NotImplementedError


#### Built-in data types #######################################################

def remove_from_string(string, letters):
    """Given an original string and a string of letters, returns a new string
    which is the same as the old one except all occurrences of those letters
    have been removed from it."""
    a = string
    b = letters
    #print(b[0])
    for i in range(len(b)):
        a = a.replace(b[i], "")
    #print(a)
    #a.replace(b, "")
    #print(a)
    return a

remove_from_string("catapult", "ata")

def compute_string_properties(string):
    b = string
    c = len(b)
    ####print(len(b))
    a1 = (c , )
    
    ####print(a1)
    b1 = []
    for i in range(len(b)):
        b1.append(b[i])
    b1.sort(reverse = True)
    ###print(b1)
    a2 = a1 + (b1,)
    ####print(a2)
    myset = set()
    for i in range(len(b)):
        myset.add(b[i])
    d2 = len(myset)
    a3 =a2 + (d2, )
    ####print(a3)
    return a3

def tally_letters(string):
    """Given a string of lowercase letters, returns a dictionary mapping each
    letter to the number of times it occurs in the string."""
    a = string
    b = {}
    for i in range(len(a)):
        if a[i] not in b:
            b.update({a[i]:0})
        for keys in b:
            if a[i] == keys:
                b[a[i]] += 1
    #print(b)
    return b


#### Functions that return functions ###########################################

def create_multiplier_function(m):
    """Given a multiplier m, returns a function that multiplies its input by m."""
    def mul(h):
        return m*h
    return mul



def create_length_comparer_function(check_equal):
    """Returns a function that takes as input two lists. If check_equal == True,
    this function will check if the lists are of equal lengths. If
    check_equal == False, this function will check if the lists are of different
    lengths."""
    def list_compare(a, b):
        if check_equal == True:
            if len(a) == len(b):
                return True
            else:
                return False
        else:
            if len(a) == len(b):
                return False
            else:
                return True
        
    return list_compare



#### Objects and APIs: Copying and modifying objects ############################

def sum_of_coordinates(point):
    """Given a 2D point (represented as a Point object), returns the sum
    of its X- and Y-coordinates."""
    return point.getX() + point.getY()

def get_neighbors(point):
    """Given a 2D point (represented as a Point object), returns a list of the
    four points that neighbor it in the four coordinate directions. Uses the
    "copy" method to avoid modifying the original point."""
    a = []
##     point.getX(), point.getY()
##    c = point.getX(), point.getY()
##    d = point.getX(), point.getY()
##    e = point.getX(), point.getY()
    c = point.copy()
    c.setX(c.getX()-1) 
    c.setY(c.getY()) 
    a.append(c)
    d = point.copy()
    d.setX(d.getX()+1) 
    d.setY(d.getY()) 
    a.append(d)
    e = point.copy()
    e.setX(e.getX()) 
    e.setY(e.getY()-1)
    a.append(e)
    f = point.copy()
    f.setX(f.getX())
    f.setY(f.getY()+1)
    a.append(f)
    #a.append
    return a



#### Using the "key" argument ##################################################

def sort_points_by_Y(list_of_points):
    """Given a list of 2D points (represented as Point objects), uses "sorted"
    with the "key" argument to create and return a list of the SAME (not copied)
    points sorted in decreasing order based on their Y coordinates, without
    modifying the original list."""
    a = list_of_points
    sorting_function = lambda point: point.getY()
    return sorted(a, key = sorting_function, reverse = True)
    #we can also do splicing [: : -1]


def furthest_right_point(list_of_points):
    """Given a list of 2D points (represented as Point objects), uses "max" with
    the "key" argument to return the point that is furthest to the right (that
    is, the point with the largest X coordinate)."""
    a = list_of_points
    maximum_function = lambda point: point.getX()
    #print(max(a, key = maximum_function))
    return max(a, key = maximum_function)


#### SURVEY ####################################################################

# How much programming experience do you have, in any language?
#     A. No experience (never programmed before this lab)
#     B. Beginner (just started learning to program, e.g. took one programming class)
#     C. Intermediate (have written programs for a couple classes/projects)
#     D. Proficient (have been programming for multiple years, or wrote programs for many classes/projects)
#     E. Expert (could teach a class on programming, either in a specific language or in general)

PROGRAMMING_EXPERIENCE = "C"


# How much experience do you have with Python?
#     A. No experience (never used Python before this lab)
#     B. Beginner (just started learning, e.g. took 6.0001)
#     C. Intermediate (have used Python in a couple classes/projects)
#     D. Proficient (have used Python for multiple years or in many classes/projects)
#     E. Expert (could teach a class on Python)

PYTHON_EXPERIENCE = "C"


# Finally, the following questions will appear at the end of every lab.
# The first three are required in order to receive full credit for your lab.

NAME = "GAURAV CHANDRA"
COLLABORATORS = "None"
HOW_MANY_HOURS_THIS_LAB_TOOK = 5
SUGGESTIONS = None #optional
