# MIT 6.034 Lab 6: Neural Nets
# Written by 6.034 Staff

from nn_problems import *
from math import e
INF = float('inf')


#### Part 1: Wiring a Neural Net ###############################################

nn_half = [1]

nn_angle = [2, 1]

nn_cross = [2,2,1]

nn_stripe = [3,1]

nn_hexagon = [6,1]

nn_grid = [4,2,1]


#### Part 2: Coding Warmup #####################################################

# Threshold functions
def stairstep(x, threshold=0):
    "Computes stairstep(x) using the given threshold (T)"
    ##print(type(threshold))
    if x>=threshold:
        return 1
    else:
        return 0
    #raise NotImplementedError

def sigmoid(x, steepness=1, midpoint=0):
    "Computes sigmoid(x) using the given steepness (S) and midpoint (M)"
    #print(type(steepness))
    #print(type(midpoint))
    #print(e(2))
    return 1/(1+e**(-steepness*(x-midpoint)))
    #raise NotImplementedError

def ReLU(x):
    "Computes the threshold of an input using a rectified linear unit."
    return max(0,x)
    #raise NotImplementedError

# Accuracy function
def accuracy(desired_output, actual_output):
    "Computes accuracy. If output is binary, accuracy ranges from -0.5 to 0."
    return -0.5*(desired_output-actual_output)**2


#### Part 3: Forward Propagation ###############################################

def node_value(node, input_values, neuron_outputs):  # PROVIDED BY THE STAFF
    """
    Given 
     * a node (as an input or as a neuron),
     * a dictionary mapping input names to their values, and
     * a dictionary mapping neuron names to their outputs
    returns the output value of the node.
    This function does NOT do any computation; it simply looks up
    values in the provided dictionaries.
    """

    if isinstance(node, str):
        # A string node (either an input or a neuron)
        if node in input_values:
            return input_values[node]
        if node in neuron_outputs:
            return neuron_outputs[node]
        raise KeyError("Node '{}' not found in either the input values or neuron outputs dictionary.".format(node))
    
    if isinstance(node, (int, float)):
        # A constant input, such as -1
        return node
    
    raise TypeError("Node argument is {}; should be either a string or a number.".format(node))

def forward_prop(net, input_values, threshold_fn=stairstep):
    # print(type(net))
    # print(input_values)
    # print(threshold_fn)
    """Given a neural net and dictionary of input values, performs forward
    propagation with the given threshold function to compute binary output.
    This function should not modify the input net.  Returns a tuple containing:
    (1) the final output of the neural net
    (2) a dictionary mapping neurons to their immediate outputs"""
    #print(net.topological_sort)
    neural_network = net.topological_sort
    #print(neural_network)

    output_values = {}
    for i in net.topological_sort():
        incoming_neighbors = net.get_incoming_neighbors(i)
        s =0 
        for j in incoming_neighbors:
            s1 = node_value(j, input_values, output_values)
            a = net.get_wires(startNode = j, endNode = i)[0]
            print(a)
            weight = a.get_weight()
            s1 = s1*weight
            s+=s1
        #print(a) 
        f = threshold_fn(s)
        output_values[i] = f
    return (output_values[net.get_output_neuron()], output_values)



#### Part 4: Backward Propagation ##############################################

def gradient_ascent_step(func, inputs, step_size):
    """Given an unknown function of three variables and a list of three values
    representing the current inputs into the function, increments each variable
    by +/- step_size or 0, with the goal of maximizing the function output.
    After trying all possible variable assignments, returns a tuple containing:
    (1) the maximum function output found, and
    (2) the list of inputs that yielded the highest function output."""
    # print(inputs)
    # print(func)
    # print(step_size)
    #for i in inputs:
    print(func(inputs[0], inputs[1], inputs[2]))
    a = {}
    a[21] = func(inputs[0], inputs[1], inputs[2])
    
 

    a[1] = func(inputs[0]-step_size, inputs[1], inputs[2])
    # if c == 1:
    #     store = ()
    a[2] = func(inputs[0], inputs[1]-step_size, inputs[2])
    a[3] = func(inputs[0], inputs[1], inputs[2]-step_size)
    a[4] = func(inputs[0]+step_size, inputs[1], inputs[2])
    a[5] = func(inputs[0], inputs[1]+step_size, inputs[2])
    a[6] = func(inputs[0], inputs[1], inputs[2]+step_size)

    a[7] = func(inputs[0]-step_size, inputs[1]-step_size, inputs[2])
    a[8] = func(inputs[0], inputs[1]-step_size, inputs[2]-step_size)
    a[9] = func(inputs[0]-step_size, inputs[1], inputs[2]-step_size)

    a[10] = func(inputs[0]+step_size, inputs[1]+step_size, inputs[2])
    a[1] = func(inputs[0], inputs[1]+step_size, inputs[2]+step_size)
    a[12] = func(inputs[0]+step_size, inputs[1], inputs[2]+step_size)

    a[13] = func(inputs[0]+step_size, inputs[1] + step_size, inputs[2]+step_size)
    a[14] = func(inputs[0]-step_size, inputs[1] - step_size, inputs[2]-step_size)

    a[15] = func(inputs[0]-step_size, inputs[1]+step_size, inputs[2])
    a[16] = func(inputs[0], inputs[1]-step_size, inputs[2]+step_size)
    a[17] = func(inputs[0]-step_size, inputs[1], inputs[2]+step_size)

    a[18] = func(inputs[0]+step_size, inputs[1]-step_size, inputs[2])
    a[19] = func(inputs[0], inputs[1]+step_size, inputs[2]-step_size)
    a[20] = func(inputs[0]+step_size, inputs[1], inputs[2]-step_size)

    a[22] = func(inputs[0]+step_size, inputs[1] - step_size, inputs[2]-step_size)
    a[23] = func(inputs[0]+step_size, inputs[1] - step_size, inputs[2]+step_size)
    a[24] = func(inputs[0]+step_size, inputs[1] + step_size, inputs[2]-step_size)
    a[25] = func(inputs[0]-step_size, inputs[1] + step_size, inputs[2]+step_size)
    a[26] = func(inputs[0]-step_size, inputs[1] - step_size, inputs[2]+step_size)
    a[27] = func(inputs[0]-step_size, inputs[1] + step_size, inputs[2]-step_size)
    # print(len(a))

    c = max(a, key=a.get)

    if c == 10:
        store = (inputs[0]+step_size, inputs[1]+step_size, inputs[2])
    if c == 25:
        store = (inputs[0]-step_size, inputs[1] + step_size, inputs[2]+step_size)
    if c == 1:
        store = (inputs[0]-step_size, inputs[1], inputs[2])
    if c== 2:
        store = (inputs[0], inputs[1]-step_size, inputs[2])
    if c== 3:
        store = (inputs[0], inputs[1], inputs[2]-step_size)
    if c== 4:
        store = (inputs[0]+step_size, inputs[1], inputs[2])
    if c== 5:
        store = (inputs[0], inputs[1]+step_size, inputs[2])
    if c== 6:
        store = (inputs[0], inputs[1], inputs[2]+step_size)
    if c==7:
        store = (inputs[0]-step_size, inputs[1]-step_size, inputs[2])
    if c== 8:
        store = (inputs[0], inputs[1]-step_size, inputs[2]-step_size)
    if c== 9:
        store = (inputs[0]-step_size, inputs[1], inputs[2]-step_size)
    if c== 11:
        store = (inputs[0], inputs[1]+step_size, inputs[2]+step_size)
    if c== 12:
        store = (inputs[0]+step_size, inputs[1], inputs[2]+step_size)
    if c == 13:
        store = (inputs[0]+step_size, inputs[1] + step_size, inputs[2]+step_size)
    if c== 14:
        store = (inputs[0]-step_size, inputs[1] - step_size, inputs[2]-step_size)
    if c == 15:
        store = (inputs[0]-step_size, inputs[1]+step_size, inputs[2])
    if c== 16:
        store =(inputs[0], inputs[1]-step_size, inputs[2]+step_size)
    if c== 17:
        store = (inputs[0]-step_size, inputs[1], inputs[2]+step_size)
    if c== 18:
        store = (inputs[0]+step_size, inputs[1]-step_size, inputs[2])
    if c== 19:
        store = (inputs[0], inputs[1]+step_size, inputs[2]-step_size)
    if c== 20:
        store =(inputs[0]+step_size, inputs[1], inputs[2]-step_size)
    if c== 21:
        store = (inputs[0], inputs[1], inputs[2])
    if c== 22:
        store = (inputs[0]+step_size, inputs[1] - step_size, inputs[2]-step_size)
    if c == 23:
        store = (inputs[0]+step_size, inputs[1] - step_size, inputs[2]+step_size)
    if c== 24:
        store = (inputs[0]+step_size, inputs[1] + step_size, inputs[2]-step_size)
    if c== 26:
        store = (inputs[0]-step_size, inputs[1] - step_size, inputs[2]+step_size)
    if c== 27:
        store = (inputs[0]-step_size, inputs[1] + step_size, inputs[2]-step_size)
    #print(max(a, key=a.get))
    #    print(a[c])
    print(c)

    return (a[c], store)




def get_back_prop_dependencies(net, wire):
    """Given a wire in a neural network, returns a set of inputs, neurons, and
    Wires whose outputs/values are required to update this wire's weight."""
    #get the wires i and j node 
    
    neural_network = net.topological_sort()
    
    print(neural_network)

    # while()
    a = set()
    a.add(wire)
    start = wire.startNode
    end = wire.endNode
    a.add(end)
    a.add(start)
    getwires = net.get_wires(startNode = end)
    print(a)

    if wire.endNode == neural_network[-1]:
        a.add(neural_network[-1])
        a.add(wire)
        a.add(wire.startNode)

        return a
        # return a
        # return a
    for i in getwires:
        a.add(i)
        a.add(i.startNode)
        a.add(i.endNode)
        print(a)
        a = a.union(get_back_prop_dependencies(net, i))
    return a

def calculate_deltas(net, desired_output, neuron_outputs):
    """Given a neural net and a dictionary of neuron outputs from forward-
    propagation, computes the update coefficient (delta_B) for each
    neuron in the net. Uses the sigmoid function to compute neuron output.
    Returns a dictionary mapping neuron names to update coefficient (the
    delta_B values). """
    d = {}
    neural_network = net.topological_sort()
    neural_network.reverse()
    for i in neural_network:
        if i == neural_network[0]:
            res = neuron_outputs[i]*(1-neuron_outputs[i])*(desired_output- neuron_outputs[i])
            d[i] = res
        else:
            a =0 
            for j in net.get_wires(i):
                a += j.get_weight()*d[j.endNode]
            res = neuron_outputs[i]*(1-neuron_outputs[i])*a
            d[i] = res

    return d
def update_weights(net, input_values, desired_output, neuron_outputs, r=1):
    """Performs a single step of back-propagation.  Computes delta_B values and
    weight updates for entire neural net, then updates all weights.  Uses the
    sigmoid function to compute neuron output.  Returns the modified neural net,
    with the updated weights."""
    deltas = calculate_deltas(net, desired_output, neuron_outputs)
    # for j in net.get_wires():
    #     if j.endNode == 
    for k in net.inputs:
        for l in net.get_wires(startNode = k):
            l.set_weight(l.get_weight()+r*node_value(k, input_values, neuron_outputs)*deltas[l.endNode])
    for i in net.topological_sort():
        for j in net.get_wires(startNode = i):
            j.set_weight(j.get_weight()+r*node_value(i, input_values, neuron_outputs)*deltas[j.endNode])
    return net

def back_prop(net, input_values, desired_output, r=1, minimum_accuracy=-0.001):
    """Updates weights until accuracy surpasses minimum_accuracy.  Uses the
    sigmoid function to compute neuron output.  Returns a tuple containing:
    (1) the modified neural net, with trained weights
    (2) the number of iterations (that is, the number of weight updates)"""
    i =0 
    actual_output, neuron_outputs = forward_prop(net, input_values, threshold_fn = sigmoid)
    print(actual_output)
    print(neuron_outputs)
    print("----")
    while accuracy(desired_output, actual_output) < minimum_accuracy:
        i+=1
        update_weights(net, input_values, desired_output, neuron_outputs, r)
        actual_output, neuron_outputs = forward_prop(net, input_values, threshold_fn = sigmoid)
        print(actual_output)
        print(neuron_outputs)
    return (net, i)


#### Part 5: Training a Neural Net #############################################

ANSWER_1 = 21
ANSWER_2 = 25
ANSWER_3 = 6
ANSWER_4 = 89
ANSWER_5 = 22

ANSWER_6 = 1
ANSWER_7 = "checkerboard"
ANSWER_8 = ["small", "medium", "large"]
ANSWER_9 = "B"

ANSWER_10 = "D"
ANSWER_11 = ["A", "C"]
ANSWER_12 = ["A", "E"]


#### SURVEY ####################################################################

NAME = "Gaurav Chandra"
COLLABORATORS = ""
HOW_MANY_HOURS_THIS_LAB_TOOK = 8
WHAT_I_FOUND_INTERESTING = ""
WHAT_I_FOUND_BORING = ""
SUGGESTIONS = ""
