# MIT 6.034 Lab 3: Games
# Written by 6.034 staff

from game_api import *
from boards import *
from toytree import GAME1

INF = float('inf')

# Please see wiki lab page for full description of functions and API.

#### Part 1: Utility Functions #################################################

def is_game_over_connectfour(board):
    """Returns True if game is over, otherwise False."""
    # print(type(board))
    # print(board.num_rows)
    # print(board.num_cols)
    #print(board.get_all_chains())
    a = board.get_all_chains()
    b = []
    for i in a:
        b.append(len(i))
    #print(max(b))
   
    if len(b) == 0:
        return False
    elif max(b) >= 4:
        return True
    else:
        if board.count_pieces() == 42:
            return True
        else:
            return False
    # if len(b) == 0:
    #     return False
    # elif max(b) >=4:             #tied
    #     return True
    # #return False 
    # else:
    #     sum = 0
    #     for i in range(board.num_cols):
    #         if board.is_column_full(i):
    #             sum += 1
    #     if sum == 7:
    #         return True

    
    # if max(len(a))==4:
    #     return True
    # return False
    # if board.get_column_height(get_column_height) != 6:
    #     return False
    # return True

def next_boards_connectfour(board):
    """Returns a list of ConnectFourBoard objects that could result from the
    next move, or an empty list if no moves can be made."""
    a = []
    if is_game_over_connectfour(board):
        return []
    else:
        for i in range(7):
            if board.is_column_full(i) == False:
                a.append(board.add_piece(i))
                #return a
            else:
                continue
    return a
    # a = board.get_all_chains()
    # if count_pieces() != 0:
    #     for i in range(6):
    #         if is_column_full(i) !=0 :
    #             board.add_piece()
    #     board.add_piece(col, row)

def endgame_score_connectfour(board, is_current_player_maximizer):
    """Given an endgame board, returns 1000 if the maximizer has won,
    -1000 if the minimizer has won, or 0 in case of a tie."""
    # print(type(is_current_player_maximizer))
    if len(max(board.get_all_chains(is_current_player_maximizer))) >=4:
        return 1000
    elif board.count_pieces() == 42:
        return 0
    else:
        return -1000
    
    
    
def endgame_score_connectfour_faster(board, is_current_player_maximizer):
    """Given an endgame board, returns an endgame score with abs(score) >= 1000,
    returning larger absolute scores for winning sooner."""
    # answer = []
    # if is_game_over_connectfour(board):
    #     a = len(board.get_all_chains(current_player = True))
    #     c = max(board.get_all_chains(current_player = True))
    #     d = max(board.get_all_chains(current_player = False))
    #     b = len(board.get_all_chains(current_player = False))
    #     print(a)
    #     print(b)  
    #     print(c)
    #     print(d)  
    #     if a > b and len(max(board.get_all_chains(current_player = True))) > len(max(board.get_all_chains(current_player = False))):
    #         answer.append(1002)
    #         answer.append(1001)
    #         return answer
    answer = 0
    a = board.get_all_chains(current_player = is_current_player_maximizer)
    #print(a)
    sum = 0
    flag =len(a[len(a)-1])
    for i in reversed(a):
        sum += 1
        if len(i)>=4:
            answer = 1100-sum
        else:
            if len(i)>= flag:
                answer = -1100 + sum

    return answer

def heuristic_connectfour(board, is_current_player_maximizer):
    """Given a non-endgame board, returns a heuristic score with
    abs(score) < 1000, where higher numbers indicate that the board is better
    for the maximizer."""
    first = is_current_player_maximizer
    if first == True:
        second = False
    else:
        second = True

    a = board.get_all_chains(current_player = first)
    times_11 = 0
    times_12 = 0
    times_13 = 0
    times_14 = 0
    for i in a:
        if len(i)==1:
            times_11+=1
        if len(i)==2:
            times_12+=1
        if len(i)==3:
            times_13+=1
        if len(i)>=4:
            times_14+=1
     
    trap = 1
    b = board.get_all_chains(current_player = second)
    times_21 = 0
    times_22 = 0
    times_23 = 0
    times_24 = 0
    for i in b:
        if len(i)==1:
            times_21+=1
        if len(i)==2:
            times_22+=1
        if len(i)==3:
            times_23+=1
        if len(i)>=4:
            times_24+=1   
    

    if times_23 > times_13:
        # print(a)
        # print(b)
        return -980 - times_23 + times_13

    elif times_13 > times_23:
        # print(a)
        # print(b)
        
        return 990+ times_23 + times_13
    else:
        if times_12 > times_22:
            return 970
        elif times_22 > times_12:
            return 960
        else:
            if times_11 > times_21:
                return 950
            else:
                return 940


# Now we can create AbstractGameState objects for Connect Four, using some of
# the functions you implemented above.  You can use the following examples to
# test your dfs and minimax implementations in Part 2.

# This AbstractGameState represents a new ConnectFourBoard, before the game has started:
state_starting_connectfour = AbstractGameState(snapshot = ConnectFourBoard(),
                                 is_game_over_fn = is_game_over_connectfour,
                                 generate_next_states_fn = next_boards_connectfour,
                                 endgame_score_fn = endgame_score_connectfour_faster)

# This AbstractGameState represents the ConnectFourBoard "NEARLY_OVER" from boards.py:
state_NEARLY_OVER = AbstractGameState(snapshot = NEARLY_OVER,
                                 is_game_over_fn = is_game_over_connectfour,
                                 generate_next_states_fn = next_boards_connectfour,
                                 endgame_score_fn = endgame_score_connectfour_faster)

# This AbstractGameState represents the ConnectFourBoard "BOARD_UHOH" from boards.py:
state_UHOH = AbstractGameState(snapshot = BOARD_UHOH,
                                 is_game_over_fn = is_game_over_connectfour,
                                 generate_next_states_fn = next_boards_connectfour,
                                 endgame_score_fn = endgame_score_connectfour_faster)


#### Part 2: Searching a Game Tree #############################################

# Note: Functions in Part 2 use the AbstractGameState API, not ConnectFourBoard.

def dfs_maximizing(state) :
    """Performs depth-first search to find path with highest endgame score.
    Returns a tuple containing:
     0. the best path (a list of AbstractGameState objects),
     1. the score of the leaf node (a number), and
     2. the number of static evaluations performed (a number)"""
    # print(state)
    # a = state.pop()
    # stack = [state]
    # a = stack.pop()
    # print(a)
    # stack = [(state, [state])]
    # (node,path) = stack.pop(0)
    # print(node, path)
    # raise NotImplementedError

    # stack=  [(state, [state])]
    # count = 0
    # while stack:
    #     (n, p) = stack.pop(0)
    #     count+=1
    #     if n.is_game_over():
    #         return (p, n.get_endgame_score(), count)
    #     else:  
    #         for i in n.generate_next_states():
    #             stack+=(i, p + [i])
        

        
    #     count+=1
    #     (nodes, path) = a.pop(0)
    #     if nodes.is_game_over():
    #         t += (path, get_endgame_score, count)
    #     for i in nodes.generate_next_states():

    # stack = [state]
    # print(stack)
    # paths= []
    # while stack:
    #     a = stack.pop(0)
    #     if 
    #     for i in state.generate_next_states():

    #     print(a)
            

    if state.is_game_over():
        return ([state], state.get_endgame_score(), 1)
    else:
        ans, count, flag = 0, 0, 0
        path=[]
        for i in state.generate_next_states():
            a = dfs_maximizing(i)
            count += a[2]
            if flag < a[1]:
                flag = a[1]
                path = [state]
                for j in a[0]:
                    path.append(j)
        return (path, flag, count)







    # print(nodes)
    # print("---------------------------")
    # print(path)
    # p.append(path)


# Uncomment the line below to try your dfs_maximizing on an
# AbstractGameState representing the games tree "GAME1" from toytree.py:

# pretty_print_dfs_type(dfs_maximizing(GAME1))


def minimax_endgame_search(state, maximize=True) :
    """Performs minimax search, searching all leaf nodes and statically
    evaluating all endgame scores.  Same return type as dfs_maximizing."""
    raise NotImplementedError


# Uncomment the line below to try your minimax_endgame_search on an
# AbstractGameState representing the ConnectFourBoard "NEARLY_OVER" from boards.py:

# pretty_print_dfs_type(minimax_endgame_search(state_NEARLY_OVER))


def minimax_search(state, heuristic_fn=always_zero, depth_limit=INF, maximize=True) :
    """Performs standard minimax search. Same return type as dfs_maximizing."""
    raise NotImplementedError


# Uncomment the line below to try minimax_search with "BOARD_UHOH" and
# depth_limit=1. Try increasing the value of depth_limit to see what happens:

# pretty_print_dfs_type(minimax_search(state_UHOH, heuristic_fn=heuristic_connectfour, depth_limit=1))


def minimax_search_alphabeta(state, alpha=-INF, beta=INF, heuristic_fn=always_zero,
                             depth_limit=INF, maximize=True) :
    """"Performs minimax with alpha-beta pruning. Same return type 
    as dfs_maximizing."""
    raise NotImplementedError


# Uncomment the line below to try minimax_search_alphabeta with "BOARD_UHOH" and
# depth_limit=4. Compare with the number of evaluations from minimax_search for
# different values of depth_limit.

# pretty_print_dfs_type(minimax_search_alphabeta(state_UHOH, heuristic_fn=heuristic_connectfour, depth_limit=4))


def progressive_deepening(state, heuristic_fn=always_zero, depth_limit=INF,
                          maximize=True) :
    """Runs minimax with alpha-beta pruning. At each level, updates anytime_value
    with the tuple returned from minimax_search_alphabeta. Returns anytime_value."""
    # anytime_value = AnytimeValue()
    # return anytime_value
    raise NotImplementedError


# Uncomment the line below to try progressive_deepening with "BOARD_UHOH" and
# depth_limit=4. Compare the total number of evaluations with the number of
# evaluations from minimax_search or minimax_search_alphabeta.

# progressive_deepening(state_UHOH, heuristic_fn=heuristic_connectfour, depth_limit=4).pretty_print()


# Progressive deepening is NOT optional. However, you may find that 
#  the tests for progressive deepening take a long time. If you would
#  like to temporarily bypass them, set this variable False. You will,
#  of course, need to set this back to True to pass all of the local
#  and online tests.
TEST_PROGRESSIVE_DEEPENING = True
if not TEST_PROGRESSIVE_DEEPENING:
    def not_implemented(*args): raise NotImplementedError
    progressive_deepening = not_implemented


#### Part 3: Multiple Choice ###################################################

ANSWER_1 = '4'

ANSWER_2 = '1'

ANSWER_3 = '4'

ANSWER_4 = '5'


#### SURVEY ###################################################

NAME = "Gaurav Chandra"
COLLABORATORS = ""
HOW_MANY_HOURS_THIS_LAB_TOOK = "3"
WHAT_I_FOUND_INTERESTING = ""
WHAT_I_FOUND_BORING = ""
SUGGESTIONS = ""
