# MIT 6.034 Lab 8: Bayesian Inference
# Written by 6.034 staff

from nets import *
import math
from collections import Counter
#### Part 1: Warm-up; Ancestors, Descendents, and Non-descendents ##############

def get_ancestors(net, var):
    "Return a set containing the ancestors of var"
    a = set()
    b = set()
    # b = a.copy()
    for i in net.get_parents(var):
        b.add(i)
    # c = len(b)
    c = list(b)
    while(len(c))>0:
        for i in c:
            a.add(i)
            if len(net.get_parents(i))>0:
                for j in net.get_parents(i): 
                    c.append(j)
                    a.add(j)
            c.remove(i)
    # a = b
    # for i in net.get_parents(var):
    #     a.add(i)
    return a

def get_descendants(net, var):
    a = set()
    b = set()
    # b = a.copy()
    for i in net.get_children(var):
        b.add(i)
    # c = len(b)
    c = list(b)
    while(len(c))>0:
        for i in c:
            a.add(i)
            if len(net.get_children(i))>0:
                for j in net.get_children(i): 
                    c.append(j)
                    a.add(j)
            c.remove(i)
    # a = b
    # for i in net.get_parents(var):
    #     a.add(i)
    return a
    "Returns a set containing the descendants of var"


def get_nondescendants(net, var):
    "Returns a set containing the non-descendants of var"
    # a = set()
    # b = set()
    # # b = a.copy()
    # for i in net.get_parents(var):
    #     b.add(i)
    # # c = len(b)
    # c = list(b)
    # while(len(c))>0:
    #     for i in c:
    #         a.add(i)
    #         if len(net.get_parents(i))>0:
    #             for j in net.get_parents(i): 
    #                 c.append(j)
    #                 a.add(j)
    #         c.remove(i)
    a = list(get_descendants(net, var))
    b = net.topological_sort()
    for i in a:
        if i in b:
            b.remove(i)
    b.remove(var)
    return set(b)
    # a = set()
    # b = set()
    # # b = a.copy()
    # for i in net.get_parents(var):
    #     b.add(i)
    # c = list(b)
    # for j in b:
    #     for k in net.get_parents(j):
    #         a.add(k)
    # return a
    # while(len(c))>0:
    #     for i in c:
    #         a.add(i)
    #         if len(net.get_parents(i))>0:
    #             for j in net.get_parents(i): 
    #                 c.append(j)
    #                 a.add(j)
    #         c.remove(i)
    # # a = b
    # # for i in net.get_parents(var):
    # #     a.add(i)


#### Part 2: Computing Probability #############################################

def simplify_givens(net, var, givens):
    """p
    If givens include every parent of var and no descendants, returns a
    simplified list of givens, keeping only parents.  Does not modify original
    givens.  Otherwise, if not all parents are given, or if a descendant is
    given, returns original givens. 
    """
    # print(givens)
    # for i in net.get_parents(var):
    #     if key in givens is i and not in list(get_nondescendants):
    #         return lambda x: x in givens and not in list(get_nondescendants)
    b = {}
    parents = 0
    descendants =0 
    # print(net)
    # print(givens)
    # print(var)
    if not givens:
        return givens
    for i in givens.keys():
        if i in net.get_parents(var):
            parents+=1
            if i not in get_descendants(net, var):
                b[i] = givens.get(i)
            if i in get_descendants(net, var):
                descendants+=1
    for i in get_descendants(net, var):
        if i in givens.keys():
            return givens
    if parents != len(net.get_parents(var)):
        return givens

    return b
    
def probability_lookup(net, hypothesis, givens=None):   
    "Looks up a probability in the Bayes net, or raises LookupError"
    # print(hypothesis)
    # a = simplify_givens(net, hypothesis.keys(), givens).keys()
    # print(a)
    # parents = 0
    # for i in net.get_parents(hypothesis.keys()):
    #     if i in a:
    #         parents +=1
    # if parents != len(net.get_parents(hypothesis.keys())):
    #     raise LookupError


    # if net.get_parents(hypothesis.keys()) != None:   
    #     return net.get_probability(hypothesis, a)
    # else:
    #     return net.get_probability(hypothesis)
    # count = 0
    # descendants = 0
    # if len(net.get_parents(list(hypothesis.keys())[0])) == 0:
    #     return net.get_probability(hypothesis)
    # else:
    #     try:
    #         Counter(list(simplify_givens(net, list(hypothesis.keys())[0], givens).keys())[0]) == Counter(net.get_parents(list(hypothesis.keys())[0])):
    #             return net.get_probability(hypothesis, simplify_givens(net, list(hypothesis.keys())[0], givens))
    if not givens:
        try:
            return net.get_probability(hypothesis)
        except ValueError:
            raise LookupError

    else:
        try:
            # print("!!")
            # if len(net.get_parents(list(hypothesis.keys())[0])) == 0:
            #     print("one")
            #     print(net.get_probability(hypothesis))
            #     return net.get_probability(hypothesis)
            
            # else:# Counter(list(simplify_givens(net, list(hypothesis.keys())[0], givens).keys())[0]) == Counter(net.get_parents(list(hypothesis.keys())[0])):
            # print("two")
            # print(net.get_probability(hypothesis, simplify_givens(net, list(hypothesis.keys())[0], givens)))
            return net.get_probability(hypothesis, simplify_givens(net, list(hypothesis)[0], givens))
            # for i in simplify_givens(net, hypothesis.keys()[0], givens).keys():
            #     if i in net.get_parents(hypothesis.keys()[0]):
            #         count +=1
            # if count == len(net.get_parents(hypothesis.keys()[0])):
            #     return net.get_probability(hypothesis, simplify_givens(net, hypothesis.keys()[0], givens))
        except ValueError:
            # print("!!!")
            raise LookupError
        # for i in simplify_givens(net, hypothesis.keys(), givens).keys():
        #     if i in givens.keys():
        #         raise LookupError
        # for i in simplify_givens(net, hypothesis.keys(), givens):
        #     if i in net.parents(hypothesis.keys()):
        #         parents+=1
        #     if i in get_descendants(net, var):
        #         descendants+=1

        # if parents == len(net.get_parents(hypothesis.keys())) and descendants == 0:
        #     return net.get_probability(hypothesis, simplify_givens(net, hypothesis.keys(), givens))
        # else:
        #     raise LookupError


def probability_joint(net, hypothesis):
    "Uses the chain rule to compute a joint probability"
    # print(hypothesis)
    # print(type(hypothesis))
    givens = hypothesis
    a = hypothesis.copy()
    newnet = net.topological_sort()
    newnet = newnet[::-1]
    p = 1
    # print(a)
    for i in newnet:
        if i in hypothesis:
            p=p*probability_lookup(net, {i:hypothesis[i]}, givens)
            del givens[i]
        # d1 = {list(newnet.keys())[0]:newnet[list(newnet.keys())[0]]}
        # # print(d1)
        # p = p*probability_lookup(newnet, d1, givens)
        # givens.update(d1)
        # del newnet[list(newnet.keys())[0]]
    return p
            
def probability_marginal(net, hypothesis):
    "Computes a marginal probability as a sum of joint probabilities"
    c = net.combinations(net.get_variables(), hypothesis)
    s = 0
    for i in c: 
        # a = dict(i, **hypothesis)
        s+= probability_joint(net, i)
    return s    

def probability_conditional(net, hypothesis, givens=None):
    "Computes a conditional probability as a ratio of marginal probabilities"

    if not givens:
        return probability_joint(net, hypothesis) 
    for i in hypothesis:
        if i in givens:
            if hypothesis[i] != givens[i]:
                return 0
    return probability_marginal(net, dict(hypothesis, **givens))/probability_marginal(net, givens)
    
def probability(net, hypothesis, givens=None):
    "Calls previous functions to compute any probability"
    # print(givens)
    if not givens:
        return probability_marginal(net, hypothesis)
    return (probability_conditional(net, hypothesis, givens))

#### Part 3: Counting Parameters ###############################################

def number_of_parameters(net):
    """
    Computes the minimum number of parameters required for the Bayes net.
    """
    # print(type(net))
    # print(len(net.get_variables()))
    # return math.pow(2, len(net.get_variables()))-1

    print(net)
    new = net.topological_sort()
    print(len(net.get_domain(net.get_variables()[0])))
    parents = []
    s = 0
    visited = set()
    for i in new:
        p= 1
        if len(net.get_parents(i))>0:
            for j in net.get_parents(i):
                p=p*len(net.get_domain(j))
        p = p*(len(net.get_domain(i))-1)
        s+=p
    return s
        # parents = []
    #     if len(net.get_parents(i))>0: 
    #         parents = []      
    #         for j in net.get_parents(i):
    #             parents.append(j)
    #         for k in parents:
    #             if k not in visited:
    #                 p+=len(net.get_domain(k))-1
    #             p=p*(len(net.get_domain(k)))
    #     #p=p+(len(net.get_domain(i))-1)
    # return p
    # if type(net.get_domain(net.get_variables()[0])[1]) == bool:
    #     return 2*len(net.get_variables())
    #     (k-1)n
    #     k^n-1
    # return n*len(net.get_variables()[])
    # else:
    #     return pow(len(net.get_variables()), len(net.get_variables()))
    # raise NotImplementedError


#### Part 4: Independence ######################################################

def is_independent(net, var1, var2, givens=None):
    """
    Return True if var1, var2 are conditionally independent given givens,
    otherwise False. Uses numerical independence.
    """
    # for i in net.get_parents(var1):
    #     if i == var2:
    #         return True
    # if net.is_neighbor(var1, var2):
    #     return True
    for i in net.get_domain(var1):
        for j in net.get_domain(var2):
            if givens != None:
                hypothesis = {var1:i, var2:j}
                a = dict({var2:j}, **givens)
                b = dict({var1:i}, **givens)
                p1 = probability(net, {var1:i}, a)
                p2 = probability(net, {var2:j}, b)

            else:
                p1 = probability(net, {var2: j})
                p2 = probability(net, {var1: i})
            c = p1*p2
            if givens != None:
                hypothesis = {var1:i, var2:j} 
                a1 = dict(hypothesis, **givens)
                if not approx_equal(c, probability(net, hypothesis, givens)):
                    return False
            else:
                if not approx_equal(c, probability(net, {var1:i, var2:j})):
                    return False

    return True
    # p1 = probability(net, var1, givens)
    # p2 = probability(net, var2, givens)
    # for i in net.get_parents(var1):
    #     if i == var2:
    #         return True
    # for i in net.get_parents(var2):
    #     if i == var1:
    #         return True
    # if approx_equal(var1, var2, epsilon = 0.01):
    #     return True
    # else:
    #     return False
    # return False

def is_structurally_independent(net, var1, var2, givens=None):
    """
    Return True if var1, var2 are conditionally independent given givens,
    based on the structure of the Bayes net, otherwise False.
    Uses structural independence only (not numerical independence).
    """
    #ancestral graph
    # print(net)
    # print(var1)
    # print(var2)
    # print(givens)
    a = set()
    a.update(get_ancestors(net, var1))
    a.update(get_ancestors(net, var2))
    a.update(var1)
    a.update(var2)
    if givens != None:
        a.update(givens)
        for i in givens:
            a.update(get_ancestors(net, i))
    # print(a)
    #moralize by marying parents
    new = net.subnet(list(a))   
    # print(new)
    newnet = new.topological_sort()
    #newnet = newnet[::-1]
    for i in newnet:
        parents = []
        if len(new.get_parents(i))>1:
            for j in new.get_parents(i):
                parents.append(j)
            new.link(parents[0], parents[1])
    #make it bidirectional
    new.make_bidirectional()
    #delete givens and their edges
    if givens != None:
        for i in givens:
            if i in new.get_variables():
                new.remove_variable(i)
    #check if they're connected
    if not new.find_path(var1, var2):
        return True
    return False



    #


#### SURVEY ####################################################################

NAME = "Gaurav Chandra"
COLLABORATORS = ""
HOW_MANY_HOURS_THIS_LAB_TOOK = 7
WHAT_I_FOUND_INTERESTING = ""
WHAT_I_FOUND_BORING = ""
SUGGESTIONS = ""
